<?php
$code;
if(isset($_GET['q']))
{
    $code = $_GET['q'];
}
else
{
    exit('{"success":false, "error":"NO_DATA" }');
}
require_once("../func/db.php");
$res = getImageByCode($code);
if($res != "no_image")
{
    exit(json_encode($res, JSON_UNESCAPED_SLASHES));
}
else
{
    exit($res);
}
?>
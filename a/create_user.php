<?php
$data;
if(isset($_POST['data']))
{
    $data = json_decode($_POST['data'],true);
}
else
{
    exit('{"success":false, "error":"NO_DATA" }');
}
require_once("../func/db.php");
$id = getCustomerID($data['name'], $data['surname'], $data['phone']);
if($id > 0)
{
    exit('{"success":true, "id":'.$id.' }');
}
$result = createUser($data['name'], $data['surname'], $data['email'], $data['phone']);
exit(json_encode($result, JSON_UNESCAPED_SLASHES));
?>
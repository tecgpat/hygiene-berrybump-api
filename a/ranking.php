<?php
require_once("../func/db.php");
require_once($_SERVER['DOCUMENT_ROOT'].'/../util_class/php/util.php');
$util = new MyUtil();
$res = getTopScorer();
for ($i=0; $i < count($res); $i++) { 
    if($res[$i]['image'] != "")
    {
        $res[$i]['image'] = $util->getFolderURL(2)."/".$res[$i]['image'];
    }
}
exit('{"list":'.json_encode($res, JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE).'}');
?>
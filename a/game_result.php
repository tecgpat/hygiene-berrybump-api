<?php
$qrDir = "qr/";
$imgDir = "images/";

//---------------------------
require_once($_SERVER['DOCUMENT_ROOT'].'/../util_class/php/util.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/../util_class/php/image_uploader.php');
$util = new MyUtil();
$url = $util->getFolderURL(2)."/";
$shareUrl = $url."share.php?q=";
$data;
$image;
if(isset($_POST['data']))
{
    $data = json_decode($_POST['data'],true);
}
else
{
    exit('NO_DATA');
}
if(!isset($_FILES['image']))
{
    exit('NO_IMAGE');
}
else
{
    $image = $_FILES['image'];
}
require_once("../func/db.php");
include('../phpqrcode/qrlib.php');

date_default_timezone_set('Asia/Bangkok');
$date = date('m/d/Y H:i:s', time());
$day = date('Y_m_d');
$target_dir = $imgDir.$day."/";
$qrOutDir = $qrDir.$day."/";

if (!is_dir("../".$target_dir))
{
	mkdir("../".$target_dir);
}
if (!is_dir("../".$qrOutDir))
{
	mkdir("../".$qrOutDir);
}

//$imageFile = $target_dir . basename($image['name']);

$upload = new ImgUploader();
$uploadRes = $upload->upload($image, "../".$target_dir);
$imageFile = $target_dir.$uploadRes['file_name'];
if ($uploadRes['success'])
{
	
} 
else 
{
    $response = array('success' => false, 
   					'error' => $uploadRes['error'],
                    'image' => $imageFile);
	exit(json_encode($response, JSON_UNESCAPED_SLASHES));
}

/*if (move_uploaded_file($image["tmp_name"], "../".$imageFile))
{
	//$imageURL .= $target_file;
	//echo "The file ". basename( $fileAttach["name"]). " has been uploaded.</br>";
    //echo "Image Type : ".$imageFileType."</br>";
} 
else 
{
    $response = array('success' => false, 
   					'error' => "uploadError",
                    'image_name' => $imageFile);
	exit(json_encode($response, JSON_UNESCAPED_SLASHES));
}*/

$result = addScore($data['id'], $data['score']);

if($result['success'] != true)
{
    $response = array('success' => false,
                    'error' => $result['error'],
                    'image' => $imageFile);
    exit(json_encode($response, JSON_UNESCAPED_SLASHES));
}

$result = addImage($data['id'], $imageFile);

if($result['success'] != true)
{
    $response = array('success' => false,
                    'error' => $result['error'],
                    'image' => $imageFile);
    exit(json_encode($response, JSON_UNESCAPED_SLASHES));
}
//---QR gen
$fileName = md5($imageFile.$date).'.png'; 
$sharePageURL = $shareUrl.$result['code'];
$pngAbsoluteFilePath = $qrOutDir.$fileName;
$urlRelativeFilePath = $url.$pngAbsoluteFilePath;
 
// generating 
if (!file_exists($pngAbsoluteFilePath)) { 
    QRcode::png($sharePageURL, "../".$pngAbsoluteFilePath, QR_ECLEVEL_L, 12);
    $qrResult = updateQR($result['code'], $pngAbsoluteFilePath);
    if($qrResult['success'] == true)
    {
        $response = array('success' => true,
                        'code' => $result['code'],
                        'image' => $url.$imageFile,
                        'share_url' => $sharePageURL,
                        'qr_url' => $urlRelativeFilePath);
        exit(json_encode($response, JSON_UNESCAPED_SLASHES));
    }
    else
    {
        $response = array('success' => false,
                        'code' => $result['code'],
                        'image' => $url.$imageFile,
                        'share_url' => $sharePageURL,
                        'qr_url' => $urlRelativeFilePath,
                        'error' => $qrResult['error']);
        exit(json_encode($response, JSON_UNESCAPED_SLASHES));
    }
}
else
{
    $response = array('success' => false,
                    'error' => 'Error creating QRcode');
    exit(json_encode($response, JSON_UNESCAPED_SLASHES));
}
?>
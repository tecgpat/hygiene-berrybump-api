<?php
$cid;
$code;
if(isset($_POST['cid']))
{
    $cid = $_POST['cid'];
}
else
{
    exit('{"success":false, "error":"NO_DATA" }');
}
if(isset($_POST['code']))
{
    $code = $_POST['code'];
}
else
{
    exit('{"success":false, "error":"NO_DATA" }');
}
require_once("../func/db.php");
$result = addShare($cid, $code);
exit(json_encode($result, JSON_UNESCAPED_SLASHES));
?>
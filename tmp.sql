--GET SUM CUSTOMER
SELECT c.id, c.name, c.surname, c.email, c.phone, IFNULL(s.score, 0) AS score, IFNULL(p.time, 0) AS play, IFNULL(r.rank, '-') AS rank, IFNULL(sh.time, 0) AS share
FROM customer AS c
LEFT OUTER JOIN (SELECT a.c_id AS cid, MAX(a.score) AS score FROM score AS a GROUP BY a.c_id) AS s ON c.id = s.cid
LEFT OUTER JOIN (SELECT a.c_id AS cid, COUNT(*) AS time FROM score AS a GROUP BY a.c_id) AS p ON c.id = p.cid
LEFT OUTER JOIN (SELECT tranking.cid, MAX(tranking.rank) AS rank 
                 FROM (SELECT tconscore.c_id AS cid, FIND_IN_SET(tconscore.score, (SELECT GROUP_CONCAT(tmaxscore.score ORDER BY tmaxscore.score DESC) 
                                                                                   FROM (SELECT toriscore.c_id AS cid, MAX(toriscore.score) AS score 
                                                                                         FROM score AS toriscore 
                                                                                         GROUP BY toriscore.c_id) AS tmaxscore)
                                                                ) AS rank 
                       FROM score AS tconscore) AS tranking
                 GROUP BY tranking.cid) AS r ON c.id = r.cid
LEFT OUTER JOIN (SELECT a.c_id AS cid, COUNT(*) AS time FROM share AS a GROUP BY a.c_id) AS sh ON c.id = sh.cid
ORDER BY c.id ASC

--GET HIGHEST RANK
SELECT c.id,CONCAT(c.name, ' ', c.surname) AS name, s.score, i.file AS image
FROM (SELECT a.c_id, a.file, MAX(a.created_time) FROM image AS a GROUP BY a.c_id) AS i
RIGHT OUTER JOIN customer AS c 
ON i.c_id = c.id
JOIN (SELECT a.c_id, MAX(a.score) AS score, a.created_time AS day FROM score AS a WHERE DATE(a.created_time) LIKE CURRENT_DATE() GROUP BY a.c_id) AS s
ON s.c_id = c.id
ORDER BY s.score DESC LIMIT 10
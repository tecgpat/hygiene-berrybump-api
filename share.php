<?php
require_once("func/db.php");
if(!isset($_GET['q']))
{
	header("Location: not_found.html");
}
require_once($_SERVER['DOCUMENT_ROOT'].'/../util_class/php/util.php');
$result = getImageByCode($_GET['q']);
$util = new MyUtil();
$url = $util->getFolderURL()."/";
if($result == "no_image")
{
	header("Location: not_found.html");
}
?>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">
<head>
	<meta charset="UTF-8">
	<title>Share Page</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta property="fb:app_id" content="506841126133506">
	<meta property="og:title" content="Test GIF">
	<meta property="og:url" content="<?php echo $url.$result['image']; ?>">
	<meta property="og:type" content="website">
	<meta property="og:site_name" content="Test GIF">
	<meta property="og:description" content="Test GIF">
	<meta property="og:image:width" content="480"/>
	<meta property="og:image:height" content="480"/>
	<meta property="og:image" content="<?php echo $url.$result['image']; ?>">
	<meta name="twitter:card" content="summary">
	<meta name="twitter:site" content="@">
	<meta name="twitter:creator" content="@">
	<meta name="twitter:title" content="Test GIF" />
	<meta name="twitter:description" content="Test GIF" />
	<meta name="twitter:image" content="<?php echo $url.$result['image']; ?>"/>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
	<link rel="stylesheet" href="css/share.css">
</head>
<body>
	<script>
		var cur = {a:"<?php echo $url.$result['image']; ?>", b:<?php echo $result['cid']; ?>, c:"<?php echo $result['code']; ?>"};
		window.fbAsyncInit = function() {
		FB.init({
			appId      : '506841126133506',
			xfbml      : true,
			version    : 'v3.1'
		});
		};
		(function(d, s, id){
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) {return;}
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/en_US/sdk.js";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>
	<div class="container-fluid pt-sm-2 pt-1">
		<div class="mx-auto text-center">
			<div class="mb-3">
				<img class="img" src="<?php echo $result['image']; ?>">
			</div>
			<div class="mt-3">
				<button class="btn btn-fb mx-3" id="share-btn"><span class="mr-2"><i class="fab fa-facebook-square"></i></span><span>Share</span></button>
				<a href="<?php echo $result['image']; ?>" download><button class="btn btn-info mx-3" id="share-btn"><span class="mr-2"><i class="fas fa-download"></i></span><span>Download</span></button></a>
			</div>
		</div>
	</div>
	<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	<script src="js/share.js"></script>
</body>
</html>
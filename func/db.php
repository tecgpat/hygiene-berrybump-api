<?php
/*ini_set('display_errors', 1);
error_reporting(E_ALL);*/
require_once($_SERVER['DOCUMENT_ROOT'].'/../util_class/php/sql_connect.php');
$db = new SQLConnect();
$db->setConfigPath($_SERVER['DOCUMENT_ROOT']."/../db/hygiene.ini");
/* require_once('g_data.php');
$gd = new global_data();
$db->setConfig(array('servername'=>$gd->getDbHost(), 'username'=>$gd->getDbUser(), 'password'=>$gd->getDbPassword(), 'dbname'=>$gd->getDbName())); */

function getImageByCode($code)
{
    global $db;
    $fields = array();
    array_push($fields, array(
        'field'=>'i.code',
        'keyword'=>$code,
        'type'=>'s'
    ));
    $result = $db->getDataWhereKey("image AS i", array("i.code", "i.c_id AS cid", "i.file AS image", "i.qr"), $fields);
    if(count($result)>0)
    {
        $response = $result[0];
    }
    else
    {
        $response = "no_image";
    }
    return $response;
}

function getTopScorer()
{
    global $db;
    $select = array('c.id',"CONCAT(c.name, ' ', c.surname) AS name", 's.score', "IFNULL(i.file, '') AS image");
    $from = "(SELECT a.c_id, a.file, MAX(a.created_time) FROM image AS a GROUP BY a.c_id) AS i
    RIGHT OUTER JOIN customer AS c 
    ON i.c_id = c.id
    JOIN (SELECT a.c_id, MAX(a.score) AS score, a.created_time AS day FROM score AS a WHERE DATE(a.created_time) LIKE CURRENT_DATE() GROUP BY a.c_id) AS s
    ON s.c_id = c.id";
    $additional = "ORDER BY s.score DESC LIMIT 10";
    $result = $db->getDataWhereKey($from, $select, array(), $additional);
    return $result;
}

function createUser($name,$surname,$email,$phone)
{
    global $db;
    $val = array(
        'id'=>0,
        'name'=>$name,
        'surname'=>$surname,
        'email'=>$email,
        'phone'=>$phone
    );
    $type = "issss";
    $result = $db->insertInto("customer",$val, $type);
    return $result;
}

function getCustomerID($name,$surname,$phone)
{
    global $db;
    $fields = array();
    array_push($fields, array(
        'field'=>'c.name',
        'keyword'=>$name,
        'type'=>'s'
    ));
    array_push($fields, array(
        'field'=>'c.surname',
        'keyword'=>$surname,
        'type'=>'s'
    ));
    array_push($fields, array(
        'field'=>'c.phone',
        'keyword'=>$phone,
        'type'=>'s'
    ));
    $result = $db->getDataWhereKey("customer AS c", array('c.id'), $fields);
    $id = 0;
    if(count($result)>0)
    {
        $id = $result[0]['id'];
    }
    return $id;
}

function addScore($cid, $score)
{
    global $db;
    $val = array(
        'id'=>0,
        'c_id'=>$cid,
        'score'=>$score
    );
    $type = "iii";
    $result = $db->insertInto("score", $val, $type);
    return $result;
}

function addImage($cid, $file)
{
    global $db;
    $code = generateCode();
    while(checkCodeExist($code))
    {
        $code = generateCode();
    }
    $val = array(
        'code'=>$code,
        'c_id'=>$cid,
        'qr'=>"",
        'file'=>$file
    );
    $type = "siss";
    $result = $db->insertInto("image", $val, $type);
    $result['code'] = $code;
    return $result;
}

function updateQR($code, $qr)
{
    global $db;
    $result = $db->updateAt("image", array('qr'=>$qr), "s", "code", $code, "s");
    return $result;
}

function generateCode()
{
    require_once($_SERVER['DOCUMENT_ROOT'].'/../util_class/php/unique_id.php');
    $str = new RandomStringGenerator();
    $code = $str->generate(10);
    return $code;
}

function checkCodeExist($code)
{
    global $db;
    $fields = array();
    array_push($fields, array(
        'field'=>'i.code',
        'keyword'=>$code,
        'type'=>'s'
    ));
    $exist = false;
    $result = $db->getDataWhereKey("image AS i", array('*'), $fields);
    if(count($result)>0)
    {
       $exist = true;
    }
    return $exist;
}

function addShare($cid, $code)
{
    global $db;
    $val = array(
        'id'=>0,
        'c_id'=>$cid,
        'img_code'=>$code
    );
    $type = "iis";
    $result = $db->insertInto("share", $val, $type);
    return $result;
}

function getSummaryUser()
{
    global $db;
    $select = array('c.id',"c.name", 'c.surname', 'c.email', "c.phone", "IFNULL(s.score, 0) AS score", "IFNULL(p.time, 0) AS play", "IFNULL(r.rank, '-') AS rank", "IFNULL(sh.time, 0) AS share");
    $from = "customer AS c
    LEFT OUTER JOIN (SELECT a.c_id AS cid, MAX(a.score) AS score FROM score AS a GROUP BY a.c_id) AS s ON c.id = s.cid
    LEFT OUTER JOIN (SELECT a.c_id AS cid, COUNT(*) AS time FROM score AS a GROUP BY a.c_id) AS p ON c.id = p.cid
    LEFT OUTER JOIN (SELECT tranking.cid, MAX(tranking.rank) AS rank 
                     FROM (SELECT tconscore.c_id AS cid, FIND_IN_SET(tconscore.score, (SELECT GROUP_CONCAT(tmaxscore.score ORDER BY tmaxscore.score DESC) 
                                                                                       FROM (SELECT toriscore.c_id AS cid, MAX(toriscore.score) AS score 
                                                                                             FROM score AS toriscore 
                                                                                             GROUP BY toriscore.c_id) AS tmaxscore)) AS rank 
                           FROM score AS tconscore) AS tranking
                     GROUP BY tranking.cid) AS r ON c.id = r.cid
    LEFT OUTER JOIN (SELECT a.c_id AS cid, COUNT(*) AS time FROM share AS a GROUP BY a.c_id) AS sh ON c.id = sh.cid";
    $additional = "ORDER BY c.id ASC";
    $result = $db->getDataWhereKey($from, $select, array(), $additional);
    return $result;
}
?>
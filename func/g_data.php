<?php
	class global_data
	{
		private $host_url = "https://tehgdev.azurewebsites.net/";
		private $db_host = "localhost";
		private $db_user = "";
		private $db_password = "";
		private $db_name = "";

		public function __construct()
		{
			foreach ($_SERVER as $key => $value) {
				if (strpos($key, "MYSQLCONNSTR_localdb") !== 0) {
					continue;
				}
				$this->db_host = preg_replace("/^.*Data Source=(.+?);.*$/", "\\1", $value);
				$this->db_name = preg_replace("/^.*Database=(.+?);.*$/", "\\1", $value);
				$this->db_user = preg_replace("/^.*User Id=(.+?);.*$/", "\\1", $value);
				$this->db_password = preg_replace("/^.*Password=(.+?)$/", "\\1", $value);
			}
		}

		public function getDbHost()
		{
			return $this->db_host;
		}

		public function getDbName()
		{
			return $this->db_name;
		}

		public function getDbUser()
		{
			return $this->db_user;
		}

		public function getDbPassword()
		{
			return $this->db_password;
		}
	}
?>
$( document ).ready(function() {
    $("#customer-table").tabulator({
        layout:"fitDataFill",
        pagination:"local",
        paginationSize:15,
        placeholder:"No Data Set",
        columns:[
            {title:"ID", field:"id", sorter:"number", align:"center"},
            {title:"ชื่อ", field:"name", sorter:"string"},
            {title:"นามสกุล", field:"surname", sorter:"string"},
            {title:"e-mail", field:"email", sorter:"string"},
            {title:"เบอร์โทรศัพท์", field:"phone", sorter:"string"},
            {title:"คะแนนสูงสุด", field:"score", sorter:"number", align:"center"},
            {title:"อันดับโดยรวม", field:"rank", align:"center", sorter:"number"},
            {title:"จำนวนที่เล่น", field:"play", align:"center", sorter:"number"},
            {title:"จำนวนที่แชร์", field:"share", align:"center", sorter:"number"}
        ],
    });
    $("#customer-table").tabulator("setData","http://localhost/hygiene/a/get_sum_customer.php");
    $("#btnDlXls").click(function(){
        $("#customer-table").tabulator("download", "xlsx", "customer_data.xlsx", {sheetName:"Customer"});
    });
    /* $("#btnDlPdf").click(function(){
        $("#customer-table").tabulator("download", "pdf", "data.pdf", {
            orientation:"landscape", //set page orientation to portrait
            title:"Berry Bump Customer Report", //add title to report
        });
    }); */

});
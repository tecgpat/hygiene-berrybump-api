<?php
/*require_once("func/db.php");
if(!isset($_GET['q']))
{
	header("Location: not_found.html");
}
require_once($_SERVER['DOCUMENT_ROOT'].'/../util_class/php/util.php');
$result = getImageByCode($_GET['q']);
$util = new MyUtil();
$url = $util->getFolderURL()."/";
if($result == "no_image")
{
	header("Location: not_found.html");
}*/
?>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">
<head>
	<meta charset="UTF-8">
	<title>Report Page</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
	<!-- <link href="vendor/tabulator/css/tabulator.min.css" rel="stylesheet"> -->
	<link href="vendor/tabulator/css/bootstrap/tabulator_bootstrap4.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/report.css">
</head>
<body>
	<div class="container-fluid pt-sm-2 pt-1">
        <div class="thead-dark table-striped" id="customer-table"></div>
        <div>
            <button id="btnDlXls" class="btn btn-success"><span class="mr-2"><i class="fas fa-file-excel"></i></span>Download Report (.xlsx)</button>
            <!-- <button id="btnDlPdf" class="btn btn-info"><span class="mr-2"><i class="fas fa-file-pdf"></i></span>Download PDF Report</button> -->
        </div>
	</div>
	<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	<script type="text/javascript" src="vendor/tabulator/js/tabulator.min.js"></script>
    <script src="https://unpkg.com/xlsx/dist/xlsx.full.min.js"></script>
    <script src="https://unpkg.com/jspdf@latest/dist/jspdf.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf-autotable/2.3.5/jspdf.plugin.autotable.js"></script>
    <script src="js/report.js"></script>
</body>
</html>